# `=` is the match operator
# it is used to assign variables
# variables can only be assigned on the left side of the match operator

singers = {:lady_gaga}
{first_singer} = singers
IO.puts("first_singer: #{first_singer}\n")

singers = {:lady_gaga, :michael_jackson}
{first_singer, second_singer} = singers
IO.puts("first_singer: #{first_singer}")
IO.puts("second_singer: #{second_singer}\n")

{first_singer, :michael_jackson} = singers
IO.puts("first_singer: #{first_singer}\n")

# {first_singer, :chimbinha} = singers
# IO.puts("first_singer: #{first_singer}\n")

# We can match mostly anything
guitarists = {"David Gilmour", "Chimbinha", "Alex Lifeson"}
# {first_guitarist, "Chimbinha"} = guitarists
{first_guitarist, "Chimbinha", _} = guitarists
IO.puts(first_guitarist <> "\n")

# Pattern matching lists
colors = ["pink", "blue", "orange"]
[head | tail] = colors
IO.inspect(head)
IO.inspect(tail)
IO.puts("")

# Pattern matching maps
bruno = %{
  coding_skills: "disappointing",
  programming_languages: %{
    "C#" => "ugh",
    "JavaScript" => "fun, but it's still JS",
    "Elixir" => "perfection"
  }
}

not_bruno = %{
  coding_skills: "absurd",
  programming_languages: %{
    "C#" => "perfection",
    "JavaScript" => "fun, but it's still JS",
    "Elixir" => "boring"
  }
}

%{coding_skills: coding_skills, programming_languages: %{"C#" => c_sharp_opinion}} = bruno
IO.inspect(coding_skills)
IO.inspect(c_sharp_opinion)
IO.puts("")

# Mixing things
brunoverse = [bruno, not_bruno]
[%{programming_languages: %{"Elixir" => elixir_opinion}} | rest] = brunoverse
IO.inspect(elixir_opinion)
IO.puts("")

# Pin operator
desired_coding_skill = "absurd"
%{coding_skills: ^desired_coding_skill, programming_languages: programming_languages} = bruno
IO.inspect(programming_languages)
