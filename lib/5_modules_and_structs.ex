defmodule Keyboard do
  # underneath, structs are simply maps
  defstruct type: "mechanical", number_of_keys: 3, has_cool_lights?: :yes

  def new do
    %Keyboard{}
  end

  # Pattern matching works on function signatures
  def new(type, number_of_keys, has_cool_lights?) do
    %Keyboard{type: type, number_of_keys: number_of_keys, has_cool_lights?: has_cool_lights?}
  end

  def are_you_a_cool_kid_based_on_your_keyboard?(%Keyboard{} = keyboard) do
    if keyboard.has_cool_lights? == :yes do
      true
    else
      false
    end
  end
end
