# Integers
my_int = 123

# Floats
my_float = 3.14

# Booleans
# Everything is truthy except for `false` and `nil`:
my_boolean = true
my_other_boolean = false

# Atoms
# An atom is a constant whose name is its value
my_atom = :atom
# `true` and `false` are also atoms

# Strings
# Strings are wrapped in double quotes
my_string = "Roundtable"
