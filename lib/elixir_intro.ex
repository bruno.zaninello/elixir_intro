defmodule ElixirIntro do
  @moduledoc """
  Documentation for `ElixirIntro`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirIntro.hello()
      :world

  """
  def hello do
    :world
  end
end
