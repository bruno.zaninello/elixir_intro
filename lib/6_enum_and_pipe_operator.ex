defmodule ListOfNumbers do
  require Integer

  def multiply_by(numbers, multiplier) do
    Enum.map(numbers, fn number -> number * multiplier end)
  end

  def divide_by(numbers, divisor) do
    Enum.map(numbers, fn number -> number / divisor end)
  end

  def filter_even(numbers) do
    Enum.filter(numbers, fn number -> Integer.is_even(number) end)
  end

  def do_it_all(numbers, multiplier, divisor) do
    numbers |> multiply_by(multiplier) |> filter_even() |> divide_by(divisor)
  end
end
