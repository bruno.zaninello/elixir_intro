# Maps
my_atom_map = %{key: "key_value"}
IO.puts(my_atom_map.key)

my_string_map = %{"key" => 280_000.983}
IO.puts(my_string_map["key"])

# Lists (they are linked lists)
my_list = [1, 2, :three, "four"]
IO.inspect(my_list)

# We can nest those structures as we like
super_data_structure = [
  %{list: [1, 2, 3], map: %{1 => "one", 2 => "two", 3 => ["three", "três", "trois"]}}
]

IO.inspect(super_data_structure)

# Tuples
my_tuple = {1, "two"}
IO.inspect(my_tuple)
my_tuple_but_bigger = {1, "two", :three, 4}
IO.inspect(my_tuple_but_bigger)
