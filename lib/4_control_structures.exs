# if
if true do
  IO.puts("That's true, bro")
else
  IO.puts("I'm not your bro, bud")
end

IO.puts("")

# case
query_result = {:ok, "We are bros"}

case query_result do
  {:ok, result} ->
    IO.puts("Everything is fine")
    IO.inspect(result)

  {:error, error} ->
    IO.puts("Houston, we've got a problem")
    IO.inspect(error)

  _ ->
    IO.puts("idc")
end

IO.puts("")
# cond
balance = 22.0

cond do
  balance == 10_000 -> IO.puts("You are very rich")
  balance * 2 > 5000 -> IO.puts("Let's buy a car")
  is_float(balance) -> IO.puts("I don't know what to say")
  true -> IO.puts("hey")
end

IO.puts("")
# with
user = %{first_name: "Pep", last_name: "Guardiola"}

with {:ok, first_name} <- Map.fetch(user, :first_name),
     {:ok, last_name} <- Map.fetch(user, :last_name) do
  IO.puts("#{first_name} #{last_name}")
end
