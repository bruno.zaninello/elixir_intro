const firstName = "Bruno";
const lastName = "Zaninello";
let fullName = "";

const impure1 = () => {
  fullName = `${firstName} ${lastName}`;
};
impure1();
console.log(fullName);

// const impure2 = (firstName, lastName) => {
//   console.log(`${lastName} ${firstName}`);
// };
// impure2(firstName, lastName);

// const pure = (firstName, lastName) => {
//   const fullName = `${firstName} ${lastName}`;

//   return fullName;
// };
// console.log("\npure!\n");
// console.log(pure(firstName, lastName));
