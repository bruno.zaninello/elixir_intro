// Declarative vs Imperative

// Let's double each number inside an array of numbers
const imperativeNumbers = [1, 2, 3, 5, 8, 13];
const delcarativeNumbers = [...imperativeNumbers];

// The imperative way
for (let i = 0; i < imperativeNumbers.length; i++) {
  const currentValue = imperativeNumbers[i];
  imperativeNumbers[i] = currentValue * 2;
}
console.log(imperativeNumbers);

// The declarative way
// console.log(delcarativeNumbers.map((number) => number * 2));
